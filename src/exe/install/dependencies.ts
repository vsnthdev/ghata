/*
 *  read the dependencies from our devDependencies and add the required
 *  ones for the adapter to Ghost and add them for the same version.
 *  Created On 08 May 2020
 */

import exec from 'execa'

import logger from '../logger'
import dependencies from '../../adapter/dependencies'

export default async function addDependencies(
    ghostPath: string,
    force: boolean,
    ghostInfo: any,
): Promise<void> {
    // read our own package.json and fetch devDependencies
    const deps = require('../../../package.json').devDependencies

    // an array of all the dependencies we will install
    const depsToInstall: string[] = []

    // loop through the dependencies specified in dependencies
    dependencies.forEach((dep: string) => {
        const version = deps[dep].replace(/[^0-9a-zA-Z.]/g, '')

        // if force is true, we install all the commands
        // else, we only issue install command for packages
        // which don't already exist
        if (force == true) {
            depsToInstall.push(`${dep}@${version}`)
        } else {
            // check if the dependency already exists
            if (!ghostInfo.dependencies[dep]) {
                depsToInstall.push(`${dep}@${version}`)
            }
        }
    })

    // prevent executing when there are not deps to add
    if (depsToInstall.length > 0) {
        logger.verbose('Installing dependencies required for ghata')
        logger.verbose(`Executing: yarn add ${depsToInstall.join(' ')}`)

        await exec('yarn', ['add', depsToInstall.join(' ')], {
            cwd: ghostPath,
        })
    }
}
